// Configure avaialable boards here.

/* 
 Eventually, the 'boards' object should be saved in a database
 and have an admin interface built to it.
*/

const boards = [
  {
    name:    'prog',
    title:   'programming',
    tagline: 'Programming related discussions.'
  },
  {
    name:    'gen',
    title:   'general',
    tagline: 'General - random conversation topics.'
  },
  {
    name:    'cyb',
    title:   'cyberpunk',
    tagline: 'Cyberpunk movies, literature, lifestyle.'
  },
  {
    name:    'mov',
    title:   'movies',
    tagline: 'Discuss the coolest movies.'
  }
];

module.exports = boards;

