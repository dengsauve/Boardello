const fs     = require('fs');
const boards = require('./boardSettings');

// 1. Read from the boards object. forEach board in boards:
// 2. Check if there's a template for the board in the 'views' directory
// 3. If there is no template, create one.
// 4. Bonus: Remove templates that are not in boards.

function createTemplate(board) {
  console.log("test");
};

boards.forEach(board => {
  fs.writeFile(
    `./views/${board.name}.pug`,
    /* 
      Template indendtation is stupid because
      Pug is sensitive to whitespace. 
      There's probably a better way to handle this
      but I haven't figured out what that is yet. 
      This is too janky for any serious or complicated layouts.
    */
    `
extends layout 
block content
  div(class="container")
    h2 ${board.tagline}
    h4 Threads:
    ul
    each thread in threads
      li
        a(href="/thread/" + thread._id, class="thread-link")= thread.title
        span updated: #{thread.updated}
  br
  div(class="post-form")
    form(action='/${board.name}/create_thread', method='post')
      label(for='title') New Thread:
      br
      input(id='title' type='text' name='title_field' placeholder='title')
      br
      lable(for='name') Name
      br
      input(id='name' type='text' name='name_field' placeholder='Anonymous')
      br
      label(for='comment') Comment
      br
      textarea(id='comment' type='text' name='comment_field' rows='20' cols='120')
      br
      input(type='submit' value='Create Thread')
    `,
    function (err) {
      if (err) {
        return console.log(err);
      }
    }
  );
});

