var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postModelSchema = new Schema({
  // Each post belongs to a thread.
  // Relate the post to a thread here?
  _postId: Schema.Types.ObjectId,
  board: String,
  thread: String,
  name: { 
    type: String,
    maxlength: 25,
    default: 'Anonymous'
  },
  date: {
    type: Date,
    default: Date.now
  },
  comment: {
    type: String,
    maxlength: 2500
  }
});

var postModel = mongoose.model('postModel', postModelSchema);

module.exports = postModel;